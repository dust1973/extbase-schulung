<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'ITS.Products',
            'Product',
            [
                'Product' => 'list, show',
                'Order' => 'new, create, userOrders',
                'User' => 'list'
            ],
            // non-cacheable actions
            [
                'Product' => 'list',
                'Order' => 'create, new, userOrders',
                'User' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    product {
                        iconIdentifier = products-plugin-product
                        title = LLL:EXT:products/Resources/Private/Language/locallang_db.xlf:tx_products_product.name
                        description = LLL:EXT:products/Resources/Private/Language/locallang_db.xlf:tx_products_product.description
                        tt_content_defValues {
                            CType = list
                            list_type = products_product
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'products-plugin-product',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:products/Resources/Public/Icons/user_plugin_product.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'ITS\\Products\\Command\\ExportOrderCommandController';