#
# Table structure for table 'tx_products_domain_model_product'
#
CREATE TABLE tx_products_domain_model_product (

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	price double(11,2) DEFAULT '0.00' NOT NULL,
	size varchar(255) DEFAULT '' NOT NULL,
	product_color int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_products_domain_model_color'
#
CREATE TABLE tx_products_domain_model_color (

	title varchar(255) DEFAULT '' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_products_domain_model_order'
#
CREATE TABLE tx_products_domain_model_order (

	user int(11) unsigned DEFAULT '0' NOT NULL,

	order_date datetime DEFAULT NULL,
	delivery_name varchar(255) DEFAULT '' NOT NULL,
	delivery_address varchar(255) DEFAULT '' NOT NULL,
	delivery_zip varchar(255) DEFAULT '' NOT NULL,
	delivery_city varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	delivery_date date DEFAULT NULL,
	price double(11,2) DEFAULT '0.00' NOT NULL,
	exported smallint(5) unsigned DEFAULT '0' NOT NULL,
	products int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (

	orders int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_products_domain_model_orderbackend'
#
CREATE TABLE tx_products_domain_model_orderbackend (

	dummy varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_products_domain_model_product'
#
CREATE TABLE tx_products_domain_model_product (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_products_product_color_mm'
#
CREATE TABLE tx_products_product_color_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_products_domain_model_order'
#
CREATE TABLE tx_products_domain_model_order (

	user int(11) unsigned DEFAULT '0' NOT NULL,

);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder