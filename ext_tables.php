<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'ITS.Products',
            'Product',
            'Produkte'
        );

        $pluginSignature = str_replace('_', '', 'products') . '_product';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:products/Configuration/FlexForms/flexform_product.xml');

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'ITS.Products',
                'web', // Make module a submodule of 'web'
                'product', // Submodule key
                '', // Position
                [
                    'OrderBackend' => 'list',
                    
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:products/Resources/Public/Icons/user_mod_product.svg',
                    'labels' => 'LLL:EXT:products/Resources/Private/Language/locallang_product.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('products', 'Configuration/TypoScript', 'Produkte');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_products_domain_model_product', 'EXT:products/Resources/Private/Language/locallang_csh_tx_products_domain_model_product.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_products_domain_model_product');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_products_domain_model_color', 'EXT:products/Resources/Private/Language/locallang_csh_tx_products_domain_model_color.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_products_domain_model_color');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_products_domain_model_order', 'EXT:products/Resources/Private/Language/locallang_csh_tx_products_domain_model_order.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_products_domain_model_order');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_products_domain_model_orderbackend', 'EXT:products/Resources/Private/Language/locallang_csh_tx_products_domain_model_orderbackend.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_products_domain_model_orderbackend');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder