<?php
namespace ITS\Products\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class OrderBackendControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \ITS\Products\Controller\OrderBackendController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\ITS\Products\Controller\OrderBackendController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllOrderBackendsFromRepositoryAndAssignsThemToView()
    {

        $allOrderBackends = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderBackendRepository = $this->getMockBuilder(\::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $orderBackendRepository->expects(self::once())->method('findAll')->will(self::returnValue($allOrderBackends));
        $this->inject($this->subject, 'orderBackendRepository', $orderBackendRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('orderBackends', $allOrderBackends);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
