<?php
namespace ITS\Products\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class OrderBackendTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \ITS\Products\Domain\Model\OrderBackend
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \ITS\Products\Domain\Model\OrderBackend();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDummyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDummy()
        );
    }

    /**
     * @test
     */
    public function setDummyForStringSetsDummy()
    {
        $this->subject->setDummy('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dummy',
            $this->subject
        );
    }
}
