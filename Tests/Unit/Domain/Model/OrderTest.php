<?php
namespace ITS\Products\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class OrderTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \ITS\Products\Domain\Model\Order
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \ITS\Products\Domain\Model\Order();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getOrderDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getOrderDate()
        );
    }

    /**
     * @test
     */
    public function setOrderDateForDateTimeSetsOrderDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setOrderDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'orderDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeliveryNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDeliveryName()
        );
    }

    /**
     * @test
     */
    public function setDeliveryNameForStringSetsDeliveryName()
    {
        $this->subject->setDeliveryName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'deliveryName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeliveryAddressReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDeliveryAddress()
        );
    }

    /**
     * @test
     */
    public function setDeliveryAddressForStringSetsDeliveryAddress()
    {
        $this->subject->setDeliveryAddress('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'deliveryAddress',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeliveryZipReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDeliveryZip()
        );
    }

    /**
     * @test
     */
    public function setDeliveryZipForStringSetsDeliveryZip()
    {
        $this->subject->setDeliveryZip('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'deliveryZip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeliveryCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDeliveryCity()
        );
    }

    /**
     * @test
     */
    public function setDeliveryCityForStringSetsDeliveryCity()
    {
        $this->subject->setDeliveryCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'deliveryCity',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeliveryDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDeliveryDate()
        );
    }

    /**
     * @test
     */
    public function setDeliveryDateForDateTimeSetsDeliveryDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDeliveryDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'deliveryDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPriceReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getPrice()
        );
    }

    /**
     * @test
     */
    public function setPriceForFloatSetsPrice()
    {
        $this->subject->setPrice(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'price',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getExportedReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getExported()
        );
    }

    /**
     * @test
     */
    public function setExportedForBoolSetsExported()
    {
        $this->subject->setExported(true);

        self::assertAttributeEquals(
            true,
            'exported',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getProductsReturnsInitialValueForProduct()
    {
        self::assertEquals(
            null,
            $this->subject->getProducts()
        );
    }

    /**
     * @test
     */
    public function setProductsForProductSetsProducts()
    {
        $productsFixture = new \ITS\Products\Domain\Model\Product();
        $this->subject->setProducts($productsFixture);

        self::assertAttributeEquals(
            $productsFixture,
            'products',
            $this->subject
        );
    }
}
