<?php
namespace ITS\Products\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class ProductTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \ITS\Products\Domain\Model\Product
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \ITS\Products\Domain\Model\Product();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPriceReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getPrice()
        );
    }

    /**
     * @test
     */
    public function setPriceForFloatSetsPrice()
    {
        $this->subject->setPrice(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'price',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getSizeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSize()
        );
    }

    /**
     * @test
     */
    public function setSizeForStringSetsSize()
    {
        $this->subject->setSize('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'size',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getProductColorReturnsInitialValueForColor()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getProductColor()
        );
    }

    /**
     * @test
     */
    public function setProductColorForObjectStorageContainingColorSetsProductColor()
    {
        $productColor = new \ITS\Products\Domain\Model\Color();
        $objectStorageHoldingExactlyOneProductColor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneProductColor->attach($productColor);
        $this->subject->setProductColor($objectStorageHoldingExactlyOneProductColor);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneProductColor,
            'productColor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addProductColorToObjectStorageHoldingProductColor()
    {
        $productColor = new \ITS\Products\Domain\Model\Color();
        $productColorObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $productColorObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($productColor));
        $this->inject($this->subject, 'productColor', $productColorObjectStorageMock);

        $this->subject->addProductColor($productColor);
    }

    /**
     * @test
     */
    public function removeProductColorFromObjectStorageHoldingProductColor()
    {
        $productColor = new \ITS\Products\Domain\Model\Color();
        $productColorObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $productColorObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($productColor));
        $this->inject($this->subject, 'productColor', $productColorObjectStorageMock);

        $this->subject->removeProductColor($productColor);
    }
}
