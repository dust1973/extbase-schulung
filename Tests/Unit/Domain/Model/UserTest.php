<?php
namespace ITS\Products\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class UserTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \ITS\Products\Domain\Model\User
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \ITS\Products\Domain\Model\User();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getOrdersReturnsInitialValueForOrder()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getOrders()
        );
    }

    /**
     * @test
     */
    public function setOrdersForObjectStorageContainingOrderSetsOrders()
    {
        $order = new \ITS\Products\Domain\Model\Order();
        $objectStorageHoldingExactlyOneOrders = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneOrders->attach($order);
        $this->subject->setOrders($objectStorageHoldingExactlyOneOrders);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneOrders,
            'orders',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addOrderToObjectStorageHoldingOrders()
    {
        $order = new \ITS\Products\Domain\Model\Order();
        $ordersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ordersObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($order));
        $this->inject($this->subject, 'orders', $ordersObjectStorageMock);

        $this->subject->addOrder($order);
    }

    /**
     * @test
     */
    public function removeOrderFromObjectStorageHoldingOrders()
    {
        $order = new \ITS\Products\Domain\Model\Order();
        $ordersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ordersObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($order));
        $this->inject($this->subject, 'orders', $ordersObjectStorageMock);

        $this->subject->removeOrder($order);
    }
}
