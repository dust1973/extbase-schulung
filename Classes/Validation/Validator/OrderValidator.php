<?php

namespace ITS\Products\Validation\Validator;

use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

class OrderValidator extends AbstractValidator
{
    /**
     * orderRepository
     *
     * @var \ITS\Products\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;

    /**
     * Check if $value is valid. If it is not valid, needs to add an error
     * to result.
     *
     * @param \ITS\Products\Domain\Model\Order $order
     */
    protected function isValid($order)
    {
        if(!$GLOBALS['TSFE']->fe_user->user['uid']
        && $this->orderRepository->countByEmail($order->getEmail())) {
            $this->addError(
                'Du hast schonmal bestellt',
                201909171422
            );
        }
        if($order->getDeliveryName() == $order->getDeliveryAddress()) {
            $this->addError(
                'Name gleich Adresse ist quatsch',
                201909171344
            );
            return false;
        }
        return true;
    }
}