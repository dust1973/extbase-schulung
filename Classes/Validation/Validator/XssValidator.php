<?php

namespace ITS\Products\Validation\Validator;

use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

class XssValidator extends AbstractValidator
{

    /**
     * Check if $value is valid. If it is not valid, needs to add an error
     * to result.
     *
     * @param mixed $value
     */
    protected function isValid($value)
    {
        if($value != strip_tags($value)) {
            $this->addError(
                'XSS is not allowed',
                201909171331
            );
            return false;
        }
        return true;
    }
}