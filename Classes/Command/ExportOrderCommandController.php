<?php
namespace ITS\Products\Command;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class ExportOrderCommandController extends CommandController
{
    /**
     * orderRepository
     *
     * @var \ITS\Products\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager = null;

    /**
     * @param string $filePath
     */
    public function exportCommand($filePath = 'fileadmin/')
    {
        $orders = $this->orderRepository->findByExported(0);
        $csv = [];
        debug($orders->count(), "Anzahl Bestellungen");

        /** @var \ITS\Products\Domain\Model\Order $order */
        foreach($orders as $order) {
            $csvRow = [];
            $csvRow[] = $order->getEmail();
            $csvRow[] = $order->getDeliveryName();
            $csv[] = implode(";", $csvRow);

            $order->setExported(true);
            $this->orderRepository->update($order);

            // direkt Speichern
            $this->persistenceManager->persistAll();
        }
        GeneralUtility::writeFile(PATH_site . str_replace("[date]", date('Ymd'), $filePath), implode("\n", $csv));
        debug(PATH_site);
        debug($filePath);
    }

    public function importCommand() {
        debug("importer");
    }
}