<?php
namespace ITS\Products\Controller;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * OrderBackendController
 */
class OrderBackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * orderRepository
     *
     * @var \ITS\Products\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;
    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $orderBackends = $this->orderRepository->findAll();
        $this->view->assign('orders', $orderBackends);
    }
}
