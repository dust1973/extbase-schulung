<?php
namespace ITS\Products\Controller;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * ProductController
 */
class ProductController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * productRepository
     * 
     * @var \ITS\Products\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository = null;

    /**
     * colorRepository
     * 
     * @var \ITS\Products\Domain\Repository\ColorRepository
     * @inject
     */
    protected $colorRepository = null;

    /**
     * action list
     * 
     * @param string $sword
     * @param string $ordering
     * @param string $ascDesc
     * @param \ITS\Products\Domain\Model\Color $color
     * @return void
     */
    public function listAction($sword = null, $ordering = 'price', $ascDesc = 'DESC', $color = null)
    {
        if ($sword) {

            // setzen der Session
            $GLOBALS['TSFE']->fe_user->setKey('ses', 'sword', $sword);
        }

        // Wert aus Session auslesen
        $sword = $GLOBALS['TSFE']->fe_user->getKey('ses', 'sword');
        $products = $this->productRepository->findByFilter($sword, $ordering, $ascDesc, $color);
        $this->view->assignMultiple(
        [
    'products' => $products,
'sword' => $sword,
'ordering' => $ordering,
'ascDesc' => $ascDesc,
'colors' => $this->colorRepository->findAll(),
'color' => $color,
'user' => $GLOBALS['TSFE']->fe_user->user
]
        );
    }

    /**
     * action show
     * 
     * @param \ITS\Products\Domain\Model\Product $product
     * @ignorevalidation $product
     * @return void
     */
    public function showAction(\ITS\Products\Domain\Model\Product $product = null)
    {
        if ($product == null) {
            $this->redirect($this->settings['redirectNoProduct']);
        }
        debug($product);
        $this->view->assign('product', $product);
    }
}
