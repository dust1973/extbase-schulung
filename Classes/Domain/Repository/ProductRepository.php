<?php
namespace ITS\Products\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * The repository for Products
 */
class ProductRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = [
    'title' => QueryInterface::ORDER_ASCENDING,
'price' => QueryInterface::ORDER_DESCENDING
];

    /**
     * Gibt Result zurück
     * 
     * @param string $sword
     * @param string $ordering
     * @param string $ascDesc
     * @param $color
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByFilter($sword, $ordering, $ascDesc, $color)
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraintsOr = [];

        // title LIKE "%[sword]%"
        $constraintsOr[] = $query->like('title', '%' . $sword . '%');

        // description LIKE "%[sword]%"
        $constraintsOr[] = $query->like('description', '%' . $sword . '%');

        // title LIKE "%[sword]%" OR description LIKE "%[sword]%"
        $constraints[] = $query->logicalOr($constraintsOr);
        if ($color) {
            $constraints[] = $query->contains('productColor', $color);
        }

        // price > 0
        $constraints[] = $query->greaterThan('price', 0);

        // WHERE (title LIKE ..... OR descriptin like ....) AND price > 0
        $query->matching($query->logicalAnd($constraints));
        $query->setOrderings(
        [
    $ordering => $ascDesc
]
        );
        return $query->execute();
    }
}
