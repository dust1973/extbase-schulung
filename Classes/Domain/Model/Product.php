<?php
namespace ITS\Products\Domain\Model;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * Product
 */
class Product extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * price
     * 
     * @var float
     */
    protected $price = 0.0;

    /**
     * size
     * 
     * @var string
     * @validate NotEmpty
     */
    protected $size = '';

    /**
     * productColor
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Color>
     */
    protected $productColor = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return "PRODUCT: " . $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the price
     * 
     * @return float $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     * 
     * @param float $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitleShort()
    {
        return substr($this->title, 0, 3);
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->productColor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Color
     * 
     * @param \ITS\Products\Domain\Model\Color $productColor
     * @return void
     */
    public function addProductColor(\ITS\Products\Domain\Model\Color $productColor)
    {
        $this->productColor->attach($productColor);
    }

    /**
     * Removes a Color
     * 
     * @param \ITS\Products\Domain\Model\Color $productColorToRemove The Color to be removed
     * @return void
     */
    public function removeProductColor(\ITS\Products\Domain\Model\Color $productColorToRemove)
    {
        $this->productColor->detach($productColorToRemove);
    }

    /**
     * Returns the productColor
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Color> $productColor
     */
    public function getProductColor()
    {
        return $this->productColor;
    }

    /**
     * Sets the productColor
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Color> $productColor
     * @return void
     */
    public function setProductColor(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $productColor)
    {
        $this->productColor = $productColor;
    }

    /**
     * Returns the size
     * 
     * @return string $size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Sets the size
     * 
     * @param string $size
     * @return void
     */
    public function setSize($size)
    {
        $this->size = $size;
    }
}
