<?php
namespace ITS\Products\Domain\Model;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * Color
 */
class Color extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * subtitle
     * 
     * @var string
     */
    protected $subtitle = '';

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';
    public function getFullTitle()
    {
        return $this->getUid() . " - " . $this->getTitle();
    }

    /**
     * Returns the subtitle
     * 
     * @return string subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     * 
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
