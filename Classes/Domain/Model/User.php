<?php
namespace ITS\Products\Domain\Model;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * User
 */
class User extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{

    /**
     * orders
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Order>
     * @cascade remove
     * @lazy
     */
    protected $orders = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->orders = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Order
     * 
     * @param \ITS\Products\Domain\Model\Order $order
     * @return void
     */
    public function addOrder(\ITS\Products\Domain\Model\Order $order)
    {
        $this->orders->attach($order);
    }

    /**
     * Removes a Order
     * 
     * @param \ITS\Products\Domain\Model\Order $orderToRemove The Order to be removed
     * @return void
     */
    public function removeOrder(\ITS\Products\Domain\Model\Order $orderToRemove)
    {
        $this->orders->detach($orderToRemove);
    }

    /**
     * Returns the orders
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Order> $orders
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Sets the orders
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\ITS\Products\Domain\Model\Order> $orders
     * @return void
     */
    public function setOrders(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $orders)
    {
        $this->orders = $orders;
    }
}
