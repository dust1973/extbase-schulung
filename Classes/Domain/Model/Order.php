<?php
namespace ITS\Products\Domain\Model;


/***
 *
 * This file is part of the "Produkte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * Order
 */
class Order extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * @var string
     */
    protected $irgendwas = null;

    /**
     * orderDate
     * 
     * @var \DateTime
     */
    protected $orderDate = null;

    /**
     * deliveryName
     * 
     * @var string
     * @validate NotEmpty
     * @validate \ITS\Products\Validation\Validator\XssValidator
     */
    protected $deliveryName = '';

    /**
     * deliveryAddress
     * 
     * @var string
     * @validate NotEmpty
     */
    protected $deliveryAddress = '';

    /**
     * deliveryZip
     * 
     * @var string
     * @validate NotEmpty
     * @validate StringLength(minimum=5, maximum=5)
     */
    protected $deliveryZip = '';

    /**
     * deliveryCity
     * 
     * @var string
     * @validate NotEmpty
     */
    protected $deliveryCity = '';

    /**
     * email
     * 
     * @var string
     * @validate NotEmpty
     * @validate EmailAddress
     */
    protected $email = '';

    /**
     * deliveryDate
     * 
     * @var \DateTime
     * @validate DateTime
     */
    protected $deliveryDate = null;

    /**
     * price
     * 
     * @var float
     */
    protected $price = 0.0;

    /**
     * exported
     * 
     * @var bool
     * @validate NotEmpty
     */
    protected $exported = false;

    /**
     * products
     * 
     * @var \ITS\Products\Domain\Model\Product
     * @lazy
     */
    protected $products = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
    }

    /**
     * Returns the orderDate
     * 
     * @return \DateTime $orderDate
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Sets the orderDate
     * 
     * @param \DateTime $orderDate
     * @return void
     */
    public function setOrderDate(\DateTime $orderDate = null)
    {
        $this->orderDate = $orderDate;
    }

    /**
     * Returns the deliveryName
     * 
     * @return string $deliveryName
     */
    public function getDeliveryName()
    {
        return $this->deliveryName;
    }

    /**
     * Sets the deliveryName
     * 
     * @param string $deliveryName
     * @return void
     */
    public function setDeliveryName($deliveryName)
    {
        $this->deliveryName = $deliveryName;
    }

    /**
     * Returns the deliveryAddress
     * 
     * @return string $deliveryAddress
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Sets the deliveryAddress
     * 
     * @param string $deliveryAddress
     * @return void
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * Returns the deliveryZip
     * 
     * @return string $deliveryZip
     */
    public function getDeliveryZip()
    {
        return $this->deliveryZip;
    }

    /**
     * Sets the deliveryZip
     * 
     * @param string $deliveryZip
     * @return void
     */
    public function setDeliveryZip($deliveryZip)
    {
        $this->deliveryZip = $deliveryZip;
    }

    /**
     * Returns the deliveryCity
     * 
     * @return string $deliveryCity
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * Sets the deliveryCity
     * 
     * @param string $deliveryCity
     * @return void
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;
    }

    /**
     * Returns the products
     * 
     * @return \ITS\Products\Domain\Model\Product $products
     */
    public function getProducts()
    {

        // Lazy-Object => laden der Daten erzwingen
        if ($this->products != null && get_class($this->products) == 'TYPO3\\CMS\\Extbase\\Persistence\\Generic\\LazyLoadingProxy') {
            $this->products->_loadRealInstance();
        }
        return $this->products;
    }

    /**
     * Sets the products
     * 
     * @param \ITS\Products\Domain\Model\Product $products
     * @return void
     */
    public function setProducts(\ITS\Products\Domain\Model\Product $products)
    {
        $this->products = $products;
    }

    /**
     * Returns the email
     * 
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     * 
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = trim(strtolower($email));
    }

    /**
     * Returns the deliveryDate
     * 
     * @return \DateTime $deliveryDate
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Sets the deliveryDate
     * 
     * @param \DateTime $deliveryDate
     * @return void
     */
    public function setDeliveryDate(\DateTime $deliveryDate = null)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * Returns the price
     * 
     * @return float $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     * 
     * @param float $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getIrgendwas()
    {
        return $this->irgendwas;
    }

    /**
     * @param string $irgendwas
     */
    public function setIrgendwas(string $irgendwas)
    {
        $this->irgendwas = $irgendwas;
    }

    /**
     * Returns the exported
     * 
     * @return bool $exported
     */
    public function getExported()
    {
        return $this->exported;
    }

    /**
     * Sets the exported
     * 
     * @param bool $exported
     * @return void
     */
    public function setExported($exported)
    {
        $this->exported = $exported;
    }

    /**
     * Returns the boolean state of exported
     * 
     * @return bool
     */
    public function isExported()
    {
        return $this->exported;
    }
}
